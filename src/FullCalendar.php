<?php

namespace romeyk\fullcalendar;

use yii\base\Widget;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
/**
 * Class Fullcalendar
 *
 * @author Roman Chuykov <mail@romeyk.org.ua>
 * @package romeyk\fullcalendar
 */
class Fullcalendar extends Widget
{
	/**
	 * @var array The fullcalendar options, for all available options check http://fullcalendar.io/docs/
	 */
	public $clientOptions = [
		'weekends' => true,
		'default'  => 'agendaDay',
		'editable' => false,
	];

	/**
	 * @var array Array containing the events, can be JSON array, PHP array or URL that returns an array containing JSON events
	 */
	public $events = [];

	/**
	 * @var array
	 * Possible header keys
	 * - center
	 * - left
	 * - right
	 * Possible options:
	 * - title
	 * - prevYear
	 * - nextYear
	 * - prev
	 * - next
	 * - today
	 * - basicDay
	 * - agendaDay
	 * - basicWeek
	 * - agendaWeek
	 * - month
	 */
	public $header = [
		'center' => 'title',
		'left'   => 'prev, next, today',
		'right'  => 'agendaDay, agendaWeek, month',
	];

	/**
	 * @var string Text to display while the calendar is loading
	 */
	public $loading = 'Please wait, calendar is loading';

	/**
	 * @var array Default options for Fullcalendar tag
	 */
	public $options = [];

	/**
	 * Always make sure we have a valid id and class for the Fullcalendar widget
	 */
	public function init()
	{
		if (!isset($this->options['id'])) {
			$this->options['id'] = $this->getId();
		}

		if (!isset($this->options['class'])) {
			$this->options['class'] = 'fullcalendar';
		}

		parent::init();
	}

	/**
	 * Load the options and start the widget
	 */
	public function run()
	{
		echo Html::beginTag('div', $this->options) . "\n";
		echo Html::endTag('div') . "\n";

		$assets = CoreAsset::register($this->view);

		if (isset($this->options['language'])) {
			$assets->language = $this->options['language'];
		}

		$this->clientOptions['header'] = $this->header;

		$js = <<<JS
			var calendar = new FullCalendar.Calendar(document.getElementById("{$this->options['id']}"), {$this->getClientOptions()});
			calendar.render();
JS;
		$this->view->registerJs($js, View::POS_READY);
	}

	/**
	 * @return string
	 * Returns an JSON array containing the fullcalendar options,
	 * all available callbacks will be wrapped in JsExpressions objects if they're set
	 */
	private function getClientOptions()
	{
		$options = ArrayHelper::merge(['events' => $this->events], $this->clientOptions);

		return Json::encode($options);
	}
}